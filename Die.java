//Polina Yankovich 1834306
import java.util.Random;
public class Die{
	private int pips;
	private Random random;
	
	public Die(){
		this.pips=1;
		this.random= new Random();		
	}
	public int getPips(){
		return this.pips;
	}
	public int roll(){
		//the range is from [0-6[
		this.pips=this.random.nextInt(6) +1 ;
	    //adding the min 1 is required to make the range [1-6]
return this.pips;
	}
public String toString(){
return " "+this.pips;
 }
 }
		